import BaseCascaderView from './BaseCascaderView.vue';
import BaseCascaderConfig from './BaseCascaderConfig.vue';

/* 级联选择框 */
class BaseCascader {
  static comName = '级联选择器';

  constructor({
    uuid = '',
    defaultValue = [],
    required = false,
    disabled = false,
    placeholder = '请选择',
    options,
  }) {
    this.type = 'BaseCascader';
    this.title = '级联选择器';
    this.uuid = uuid;
    this.defaultValue = defaultValue;
    this.required = required;
    this.disabled = disabled;
    this.placeholder = placeholder;
    this.options = options;
    this.viewComponent = BaseCascaderView;
    this.configComponent = BaseCascaderConfig;
  }
}

export default BaseCascader;

export { BaseCascaderView, BaseCascaderConfig };
